import com.company.Consumer;
import mathematica.Kubator;
import mathematica.Kvadrator;
import mathematica.Prostator;

import java.util.Collection;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Consumer consumer = new Consumer();
        for (int i = 0; i < 10; i++) {
            int[] arr = {2, 2, 2};
            Prostator prostator = new Prostator(arr, consumer);
            Kvadrator kvadrator = new Kvadrator(arr, consumer);
            Kubator kubator = new Kubator(arr, consumer);
            Thread threadProst = new Thread(prostator);
            Thread threadKvadr = new Thread(kvadrator);
            Thread threadKub = new Thread(kubator);
            threadProst.start();
            threadKub.start();
            threadKvadr.start();
        }
    }
}
