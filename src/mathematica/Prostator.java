package mathematica;

import com.company.Consumer;

/**
 * Created by sa on 10.02.17.
 */
public class Prostator extends Thread{
    int[] numbers;
    Consumer consumer;

    public Prostator(int[] numbers, Consumer consumer) {
        this.numbers = numbers;
        this.consumer = consumer;
    }

    public Prostator() {
    }

    @Override
    public void run() {
        int sum = 0;
        for (int num:
                numbers) {
            sum += num;
            consumer.message(0, 0, sum);
        }
    }
}
