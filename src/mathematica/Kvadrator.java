package mathematica;

import com.company.Consumer;

/**
 * Created by sa on 10.02.17.
 */
public class Kvadrator extends Prostator{
    int[] numbers ;
    Consumer consumer;


    public Kvadrator(int[] numbers, Consumer consumer) {
        this.numbers = numbers;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int num:
                numbers) {
            sum += Math.pow(num, 2);
            consumer.message(0, sum, 0);
        }
    }

}
