package com.company;

/**
 * Created by sa on 10.02.17.
 */
public class Consumer {
    volatile boolean semaforeKube = false;
    volatile boolean semaforeKvadr = false;
    volatile boolean semaforeChislo = false;
    int chisloSum = 0;
    int kvadratSum = 0;
    int kubeSum = 0;

    public synchronized void message(int kybe, int kvadrat, int chislo){
        if (kybe != 0){
            while(semaforeKube){Thread.yield();}
            kubeSum += kybe;
            semaforeKube = true;
        }
        else if (kvadrat != 0){
            while(semaforeKvadr){Thread.yield();}
            kvadratSum += kvadrat;
            semaforeKvadr = true;
        }
        else if (chislo != 0){
            while(semaforeChislo){Thread.yield();}
            chisloSum += chislo;
            semaforeChislo = true;
        }
        System.out.println("kube = " + kubeSum + " kvadrat = " + kvadratSum + " chislo = " + chisloSum);
        if (kybe != 0){
            semaforeKube = false;
        }
        else if (kvadrat != 0){
            semaforeKvadr = false;
        }
        else if (chislo != 0){
            semaforeChislo = false;
        }
    }
}
